# GES - Front End .Net / Angular Developer Exercise. #

### Create a web application to display a 5 day weather forecast. ###

The solution will require a .Net server application that fetches a weather forecast from a publicly available Weather API and passes it to be displayed on a Web page. 

There are a large number of publicly available APIs available here, http://openweathermap.org/api, any API of your choice can be used. However, access to the service must be from the .Net layer of your application.

The weather forecast should then be displayed in an Angular web app. It should display the expected forecast for 5-days. The data sent from the .Net service to the front end application should be sent in camel-case, such that all data fields in the JavaScript are in camel-case.

Tests for the application should be added where you feel they are appropriate.

The solution will be judged on code quality and maintainability.

Your response should be in the form of a zipped visual studio solution including only your code, i.e. dll's, nuget packages or node modules should not be included.